#Answers of String Related Questions
Answer_Key = { 
              12 : 3,

              11 : 0,

              27 : -7,

              "pigeon" : "humongous",

              "parot" : "eutopia",

              "book" : "literature",

              "Abracadabra" : "Abrakadabra",

               "Data Vcience" : "Data Science",

               "123456779" : "123456789",

               "ab43h3nks9s0vhd0" : 387209,

               "hlof9kmt50blb9m6" : 95096,

               "dkv0dfg04mg0gmd0" : 5,

               "He" : {'He': 3, 'raced': 3, 'to': 4, 'the': 3, 'grocery': 2, 
                      'store.': 1, 'went': 1, 'inside': 1, 'but': 1, 'realized': 1, 
                      'he': 3, 'forgot': 1, 'his': 1, 'wallet.': 1, 'back': 2, 
                      'home': 1, 'grab': 1, 'it.': 1, 'Once': 1, 'found': 1, 'it,': 1, 
                      'car': 1, 'again': 1, 'and': 1, 'drove': 1, 'store': 1},

                "Fear" : {'Fear': 1, 'leads': 4, 'to': 4, 'anger': 2, 'and': 3, 
                         'hatred': 2, 'conflict': 2, 'suffering': 1},

                 "Now" : {'Now': 4, 'is': 4, 'the': 9, 'time': 4, 'to': 6, 'make': 2,
                        'real': 1, 'promises': 1, 'of': 6, 'democracy.': 1, 'rise': 1,
                        'from': 2, 'dark': 1, 'and': 1, 'desolate': 1, 'valley': 1, 
                        'segregation': 1, 'sunlit': 1, 'path': 1, 'racial': 2, 
                        'justice.': 1, 'lift': 1, 'our': 1, 'nation': 1, 'quicksands': 1,
                        'injustice': 1, 'solid': 1, 'rock': 1, 'brotherhood.': 1,
                        'justice': 1, 'a': 1, 'reality': 1, 'for': 1, 'all': 1,
                        "God's": 1, 'children': 1}
                }


#Comaprison1.1
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_CountHappiness_TestCase(func, **Inputs):

  #Correct Output
  Correct_output = Answer_Key[len(Inputs['input_lst'])]

  #Running your function
  Your_output = func(Inputs['input_lst'], Inputs['lset'], Inputs['dset'])

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


#Comaprison1.2
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_Mutate_String_TestCase(func, **Inputs):
  
  #Correct Output
  Correct_output = Answer_Key[Inputs['String']]

  #Running your function
  Your_output = func(Inputs['String'], Inputs['Ch'], Inputs['Pos'])

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


#Comaprison2.1
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_HighestVowelScore_TestCase(func, Input):

  #Correct Output
  Correct_output = Answer_Key[Input[0]]

  #Running your function
  Your_output = func(Input)

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


#Comaprison2.2
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_LowestNumberInString_TestCase(func, Input):

  #Correct Output
  Correct_output = Answer_Key[Input[0]]

  #Running your function
  Your_output = func(Input)

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


#Comaprison3.1
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_CountFreq_TestCase(func, Input):
  
  #Correct Output
  Correct_output = Answer_Key[Input.split()[0]]

  #Running yout function
  Your_output = func(Input)

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
