# Answer Key
Answer_Key = {'Top7' : ['Delhi', 'Patna', 'Ahmedabad', 'Gurugram', 'Luckhnow', 'Mumbai', 'Kolkata'],
              'Percentages' : {'Delhi': 26.98, 'Patna': 11.75, 'Ahmedabad': 11.85, 'Gurugram': 15.43,
                               'Luckhnow': 0.0, 'Mumbai': 1.74, 'Kolkata': 5.92},
              'Most_Unhealthy_City' : 'Delhi'}

# Function check the given solution
def CheckingAnswer(Ques, Input):

  Answer = Answer_Key[Ques]

  if Input == Answer:
    flag = "Correct"
  else:
    flag = "Incorrect"

  print(f"The Given Solution is {flag}\n")
  print(f"Given Soluion: {Input}")
  print(f"Correct Solution: {Answer}")
