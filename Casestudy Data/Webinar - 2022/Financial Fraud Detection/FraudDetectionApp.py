import streamlit as st
import pandas as pd
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from PIL import Image

st.set_page_config(layout="wide")

#image = Image.open('/content/Header.png')
#st.image(image)

st.write("""
# Ethereum Fradulent Transaction Cheker
This app predicts the Probability of **Fradulent Ethereum Transaction!**
""")

st.sidebar.header('User Input Parameters')

def user_input_features():

    MinBetweenSent = st.sidebar.slider('Avg. Time between Sent Transaction(Mins)', 0, 500000, 320000, 100)
    MinBetweenRecieved = st.sidebar.slider('Avg. Time between Recieved Transaction(Mins)' , 0, 500000, 289000, 100)
    TimeDiff = st.sidebar.slider('Time Diff. between first and last Transaction(Mins)',0 , 2000000, 985650, 1500)
    SentTNX = st.sidebar.slider('Number of Sent Transaction ever performed', 0 , 10000, 5680, 10)
    RecTNX = st.sidebar.slider('Number of Recieved Transaction', 0 , 10000, 5680, 10)
    NumCrContract = st.sidebar.slider('Number of Contract Created', 0 , 10000, 5680, 10)
    MaxValRev = st.sidebar.slider('Maximum Value ever received in the account', 0 , 800000, 512600, 1000)  
    AvgValRev = st.sidebar.slider('Avergae Value received in the account', 0 , 300000, 112600, 1000)  
    AvgValSent = st.sidebar.slider('Avergae Value sent from the account', 0 , 200000, 112600, 1000)  
    TotEthSent = st.sidebar.slider('Total Ether sent from the account', 0 , 30000000, 12450000, 10000)  
    TotBal = st.sidebar.slider('Total Ether Balance in the account',-200000, 200000, 10563, 100)

    inputs = {'Avg min between sent tnx': MinBetweenSent,
            'Avg min between received tnx': MinBetweenRecieved,
            'Time Diff between first and last (Mins)' : TimeDiff,
            'Sent tnx':  SentTNX ,
            'Received Tnx': RecTNX,
            'Number of Created Contracts': NumCrContract,
            'max value received ' : MaxValRev,
            'avg val received': AvgValRev,
            'avg val sent': AvgValSent,
            'total Ether sent': TotEthSent,
            'total ether balance': TotBal}

    return inputs

inputs = user_input_features()
df = pd.DataFrame(inputs, index=['Parameters'])

st.subheader('User Input parameters')
st.write(df)

path = 'https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Webinar%20-%202022/Financial%20Fraud%20Detection/EthereumTNX_Data.csv'
data = pd.read_csv(path)

X = data.drop(columns=['FLAG'])
y = data.FLAG

clf = RandomForestClassifier(max_depth=10, n_estimators=100)
clf.fit(X, y)

prediction = clf.predict(df)
prediction_proba = clf.predict_proba(df)

prediction = ["Fradulent Transaction" if pred == 1 else "The Transaction is Alright" for pred in prediction]

st.subheader('Model Prediction:')
st.write(f"### {prediction[0]}")


st.subheader('Details: Prediction Probability')
Fradulent = f'{prediction_proba[0][1]: .2f}'
NonFradulent = f'{prediction_proba[0][0]: .2f}'

result = pd.DataFrame({'Not Fradulent' : NonFradulent, 'Fradulent' : Fradulent}, index=['Probability'])

st.write(result)
