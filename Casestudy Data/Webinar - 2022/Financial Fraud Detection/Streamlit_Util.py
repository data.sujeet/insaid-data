def Streamlit_Setup():
  import os
  import time

  print("Downloading Streamlit")
  #Install Streamlit
  os.system('pip install -q streamlit')
  print('Streamlit Download Finished')

  print("\nDownloading Ngrok and server side files")
  #App.py
  os.system('wget -q https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Webinar%20-%202022/Financial%20Fraud%20Detection/FraudDetectionApp.py')

  #Download Header
  '''os.system('wget -q https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Webinar%20-%202022/Data%20Science%20In%20Finance/Header.png')'''

  #ngrok
  os.system('wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip')

  #Unzip ngrok
  os.system('unzip -q ngrok-stable-linux-amd64.zip')

  #AuthFile
  os.system('wget -q https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Miscel.%20Scripts/Streamlit_auth_token.txt')

  time.sleep(2)
  print("Download Complete")
  with open('/content/Streamlit_auth_token.txt', 'r') as f:
    auth_token = f.readline().rstrip()

  import os
  os.system('./ngrok authtoken ' + auth_token)

  time.sleep(3)
  print("\nAuthentication Complete")
