import streamlit as st
import pandas as pd
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from PIL import Image


#Feature Encoder
Encoder = {'Gender' : {'Female' : 0, 'Male': 1},
           'Married': {'Not Married': 0, 'Married': 1},
           'Dependents': {'0': 0, '1': 1, '2': 2, '3/+3': 3},
           'Education': {'Graduted' : 0, 'Not Graduted': 1},
           'Self_Employed': {'No': 0, 'Yes':1},
           'Credit_History': {'No': 0, 'Yes': 1},
           'Property_Area': {'Rural': 0, 'Semi-urban': 1, 'Urban': 2}}

st.set_page_config(layout="wide")

image = Image.open('/content/Header.png')
st.image(image)

st.write("""
# Loan Eligibility Prediction App
This app predicts the **Eligibility of an applicant** for Loan!
""")

st.sidebar.header('User Input Parameters')

def user_input_features():

    Gender = st.sidebar.radio('Your Gender',options=['Female', 'Male'])
    Married = st.sidebar.radio('Marital Status',options=['Not Married', 'Married'])
    Education = st.sidebar.radio('Education Status',options=['Graduted', 'Not Graduted'])
    Self_Employed = st.sidebar.radio('Are you self employed',options=['Yes', 'No'])
    Credit_History = st.sidebar.radio('Have you ever taken any loan before',options=['No', 'Yes'])
    Property_Area = st.sidebar.radio('What kind of area you live in',options=['Rural', 'Semi-urban', 'Urban'])
    Dependents = st.sidebar.select_slider('Number of Dependents',options=['0', '1', '2', '3/+3'], value='2')  
    ApplicantIncome = st.sidebar.slider('Annual Income of Applicant (Dollars)',500, 80000, 15000, 200)
    CoapplicantIncome = st.sidebar.slider('Annual Income of Co-Applicant (Dollars)',500, 80000, 15000, 200)
    LoanAmount = st.sidebar.slider('Required Loan Amount (Dollars)',5, 1500, 200, 5)
    Loan_Amount_Term = st.sidebar.slider('Loan Amount Term (In Days)',10, 360, 85, 5)

    inputs = {'Gender': Gender,
            'Married': Married,
            'Dependents': Dependents,
            'Education': Education,
            'Self_Employed': Self_Employed,
            'ApplicantIncome': ApplicantIncome,
            'CoapplicantIncome' : CoapplicantIncome,
            'LoanAmount': LoanAmount,
            'Loan_Amount_Term': Loan_Amount_Term,
            'Credit_History': Credit_History,
            'Property_Area': Property_Area}

    return inputs

inputs = user_input_features()
df = pd.DataFrame(inputs, index=['Parameters'])

st.subheader('User Input parameters')
st.write(df)

for col in Encoder.keys():
  df[col] = df[col].apply(lambda x : Encoder[col][x])

path = 'https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Webinar%20-%202022/Data%20Science%20In%20Finance/Loan_Eligibility_Data.csv'
data = pd.read_csv(path)

X = data.drop(columns=['Loan_Amount'])
y = data.Loan_Amount

clf = RandomForestClassifier(max_depth=11, min_samples_leaf=4, min_samples_split=17, n_estimators=114)
clf.fit(X, y)

prediction = clf.predict(df)
prediction_proba = clf.predict_proba(df)

prediction = ["You're Eligible For This Loan" if pred == "Y" else "Sorry, You're Not Eligible" for pred in prediction]

st.subheader('Model Prediction:')
st.write(f"### {prediction[0]}")


st.subheader('Details: Prediction Probability')
no = f'{prediction_proba[0][1]: .2f}'
yes = f'{prediction_proba[0][0]: .2f}'

result = pd.DataFrame({'Not Eligible' : no, 'Eligible' : yes}, index=['Probability'])

st.write(result)
