import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output
import calendar
import matplotlib.pyplot as plt
import matplotlib
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd
import numpy as np

#Div Style Template
main_div_style = {"background-color": "#FFFFFF", "padding":"0", "width":"100%", "height":"100", "position": "fixed",
                  "top": "0%","left": "0","bottom": "0","maxHeight": "300px", 'overflowY': 'auto'}

#creating options for dropdown display
options = [{"label": month[:3], "value": month[:3]} for month in list(calendar.month_name)[1:]]

#Inputs and Outputs
outputs, inputs = ["bestmonth", "store_average", "week_sales", "dept_average"],  ["month_comp", "month_base"]


#Function to define callback
def Callback(outputs = outputs, inputs = inputs):

  out_ls = []
  inp_ls = []

  for output in outputs:
    out_ls.append(Output(component_id = output, component_property = "figure"))

  for input in inputs:
    inp_ls.append(Input(component_id = input, component_property = "value"))
  return out_ls, inp_ls

#Function to decompose datetime
def Decompose_Datetime(df,name):

  #Month
  df['Month'] = df[name].dt.month

  #Month
  for i, month in enumerate(calendar.month_name):
    df.loc[df['Month'] == i, 'MonthName'] = month[:3]

  #Year
  df['Year'] = df[name].dt.year

  #Week of Month
  df['WeekofMonth'] = df[name].apply(lambda d: (d.day - 1) // 7 + 1)

  return None

#Function to define layout
def Def_Layout(children=None):

  global main_div_style

  if children is None:
    return None
  else:
    return html.Div(id = "main_div", children=children, style = main_div_style)

#function to define layout
def Create_Graph_Canvas(n=None):

  canvas = html.Div(children = [
                            html.Div(children = [dcc.Graph(id = "bestmonth")],
                             style = {'width': '45%', "position": "fixed", "left": "3%", 'display': 'inline-block', "background-color": "#FFFFFF", "top": "11%",}),

                            html.Div(children = [dcc.Graph(id = "store_average")],
                             style = {'width': '45%', "position": "fixed", "left": "52%", 'display': 'inline-block', "background-color": "#FFFFFF", "top": "9.5%"}),

                            html.Div(children = [dcc.Graph(id = "week_sales")],
                             style = {'width': '45%', "position": "fixed", "left": "3%", 'display': 'inline-block', "background-color": "#FFFFFF", "top": "54%",}),

                            html.Div(children = [dcc.Graph(id = "dept_average") ],
                             style = {'width': '45%', "position": "fixed", "left": "52%",'display': 'inline-block', "background-color": "#FFFFFF","top": "54%"})
                            ])
  return canvas

#function to create a dropdown
def Create_Dropdown_Box(id, default_value, options = options, style=None, label = None):

  def_style = {'width': '28%', "position": "fixed","left": "18%",'display': 'inline-block', "top": "5%", "z-index": "1"}

  if style is not None:
    for k,v in style.items():
      def_style[k] = v
  else:
    pass

  ddbox = html.Div(children = [html.Label([label], style={'font-weight': 'bold', "text-align": "left"}),
            dcc.Dropdown(
                    id=id,
                    options=options,
                    clearable=False,
                    value = default_value,
                    placeholder='Select a Month')],
            style = def_style)
  return ddbox

#Driver function to added almost everything
def Setup_DashApp():

  #Heading
  Heading = html.Div([html.H2('Sales Comparison Dashboard', style={'margin-left' : '38%', 'margin-top' : '0.1%', 'color': '#003399', 'font-family' : 'Helvetica'})])

  #creating dropdown sections
  Month_comp_box = Create_Dropdown_Box(id = 'month_comp', default_value = 'Jan', label='Base Month (Blue)')
  Month_base_box = Create_Dropdown_Box(id = 'month_base', style = {"left": "51%"}, default_value = 'Dec', label = 'Comparison Month (Red)')

  #Setting up graphs
  Graphs = Create_Graph_Canvas()

  return [Heading, Month_comp_box, Month_base_box, Graphs]

#Hidden Functons to make the exact plot
def Figure1(data, *args):

  #Finding Month with highest average sales
  month_avg = data.groupby('MonthName').mean().sort_values(by='Weekly_Sales', ascending=False)[:10]

  #Plot
  fig = Pie_plot(month_avg, column='Weekly_Sales')

  #show
  return fig

def Figure2(data, month_comp, month_base='Dec'):

  #Calculating Weekly Sales
  weekly_sales = data.groupby(['MonthName','WeekofMonth']).mean()

  #Plotting Feb vs Dec
  fig = Line_plot(weekly_sales, month_comp, month_base)

  #show
  return fig

def Figure3(data, month_comp, month_base='Dec'):

  #Grouping by Month and Store
  Store_by_month = data.groupby(['MonthName','Store']).sum()
  #Top Store for Feb
  Store_comp = Store_by_month.loc[month_comp].sort_values(by='Weekly_Sales',ascending=False)[:10]
  #Top Store for Dec
  Store_base = Store_by_month.loc[month_base].sort_values(by='Weekly_Sales',ascending=False)[:10]

  #plotting
  fig = make_subplots(rows=1, cols=2)

  fig.add_trace(go.Bar(x=Store_comp['Weekly_Sales'], y=Store_comp.index,  orientation='h', name=month_comp, text = Store_comp['Weekly_Sales']), row=1, col=1)
  fig.add_trace(go.Bar(x=Store_base['Weekly_Sales'], y=Store_base.index, orientation='h', name=month_base, text = Store_base['Weekly_Sales']), row=1, col=2)
  Update_layout(fig, 'Stores', month_comp, month_base)

  return fig

def Figure4(data, month_comp, month_base='Dec'):

  #Sorting by Month and Dept
  Dept_by_month = data.groupby(['MonthName','Dept']).sum()
  #Top Dept for Feb
  Dept_comp = Dept_by_month.loc[month_comp].sort_values(by='Weekly_Sales',ascending=False)[:10]
  #Top Dept for Dec
  Dept_base = Dept_by_month.loc[month_base].sort_values(by='Weekly_Sales',ascending=False)[:10]

  #plotting
  fig = make_subplots(rows=1, cols=2)
  fig.add_trace(go.Bar(x=Dept_comp['Weekly_Sales'], y=Dept_comp.index,  orientation='h', name=month_comp, text = Dept_comp['Weekly_Sales']), row=1, col=1)
  fig.add_trace(go.Bar(x=Dept_base['Weekly_Sales'], y=Dept_base.index, orientation='h', name=month_base, text = Dept_base['Weekly_Sales']), row=1, col=2)
  Update_layout(fig, 'Department', month_comp, month_base)

  #Show
  return fig

#function for Pie plot
def Pie_plot(month_avg, column):

  fig = px.pie(month_avg, values=column, names=month_avg.index, hole=.3)
  fig.update_layout(width=900, height=500, template='plotly_white', title='Average Sales in Each Month')
  fig.update_traces(pull= [0.2] + [0]*11 )

  return fig

#function for Line plot
def Line_plot(weekly_sales, month_comp, month_base):

  fig = go.Figure(go.Scatter(x=weekly_sales.loc[month_comp].index, y=weekly_sales.loc[month_comp].Weekly_Sales, name=month_comp))
  fig.add_trace(go.Scatter(x=weekly_sales.loc[month_base].index, y=weekly_sales.loc[month_base].Weekly_Sales, name=month_base))
  fig.update_layout(width=900, height=500, template='plotly_white', title='Average Sales Comparison Between {} and {} (Weekly)'.format(month_comp, month_base))
  fig.update_xaxes(type='category', title='Week of Month')

  return fig

#function to update bar plot layout
def Update_layout(fig, ytitle, month_comp, month_base):

  fig.update_yaxes(type='category', title= ytitle)
  fig.update_layout(width=900, height=500, template='plotly_white', title='Comparison of Best {} in {} and {}'.format(ytitle, month_comp, month_base))
