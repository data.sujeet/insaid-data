def Streamlit_Setup():
  import os
  import time

  print("Downloading Streamlit")
  #Install Streamlit
  os.system('pip install -q streamlit')
  print('Streamlit Download Finished')

  print("\nDownloading Ngrok and server side files")

  #App.py
  os.system('wget -q https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Webinar%20-%202022/Surge%20Price%20Prediction/SurgePredictionApp.py')

  #Model pickle
  os.system('wget -q https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Webinar%20-%202022/Surge%20Price%20Prediction/Saved_Model.pkl')

  #Encoder pickle
  os.system('wget -q https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Webinar%20-%202022/Surge%20Price%20Prediction/Encoder.pkl')

  #App Config file
  os.system('wget -q https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Webinar%20-%202022/Surge%20Price%20Prediction/config.toml -P /content/.streamlit')

  #App Icon
  os.system('wget -q https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Webinar%20-%202022/Surge%20Price%20Prediction/App_Icon.png')

  #ngrok
  os.system('wget -q https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip')

  #Unzip ngrok
  os.system('unzip -q ngrok-stable-linux-amd64.zip')

  #AuthFile
  os.system('wget -q https://gitlab.com/academics3/insaid-data/-/tree/main/Casestudy%20Data/Miscel.%20Scripts/Streamlit_auth_token.txt')

  time.sleep(2)
  print("Download Complete")
  with open('/content/Streamlit_auth_token.txt', 'r') as f:
    auth_token = f.readline().rstrip()

  import os
  os.system('./ngrok authtoken ' + auth_token)

  time.sleep(3)
  print("\nAuthentication Complete")
