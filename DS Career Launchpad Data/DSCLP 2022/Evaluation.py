
#Answers of String Related Questions
Answer_Key = { 
              12 : 3,

              11 : 0,

              27 : -7,

              "pigeon" : "humongous",

              "parot" : "eutopia",

              "book" : "literature",
              
                "Dewvrak12@hotmail.com" : ['hotmail', 'gmail', 'yahoo', 'asia'],

                "kspiteri@outlook.com" : ['outlook', 'sbcglobal', 'me', 'aol', 'verizon', 'yahoo', 'gmail', 'msn', 'live']
                }


#Comaprison1.1
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_CountHappiness_TestCase(func, **Inputs):

  #Correct Output
  Correct_output = Answer_Key[len(Inputs['input_lst'])]

  #Running your function
  Your_output = func(Inputs['input_lst'], Inputs['lset'], Inputs['dset'])

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


#Comaprison2.1
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_HighestVowelScore_TestCase(func, Input):

  #Correct Output
  Correct_output = Answer_Key[Input[0]]

  #Running your function
  Your_output = func(Input)

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

#Comaprison3.1
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_UnqiueDomains_TestCase(func, Input):

  #Correct Output
  Correct_output = Answer_Key[Input.split()[0]]

  #Running your function
  Your_output = func(Input)

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
